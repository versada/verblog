from django.contrib import admin
from models import Entry
from forms import EntryForm


class EntryAdmin(admin.ModelAdmin):
    form = EntryForm
    exclude = ('author',)
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title', 'is_published', 'pub_date', 'author')
    fieldsets = (
        (None, {
            'fields': ('title', 'slug', 'is_published', 'description', 'body',
                       'image', 'tags')
        }),
        ('Advanced options', {
            'classes': ('collapse',),
            'fields': ('pub_date', )
        }),
    )

    def save_model(self, request, obj, form, change):
        if not change:
            obj.author = request.user
        obj.save()

admin.site.register(Entry, EntryAdmin)