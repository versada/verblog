from django.db import models
from taggit.managers import TaggableManager
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils import timezone


class Entry(models.Model):
    title = models.CharField(max_length=254, blank=False)
    slug = models.SlugField(max_length=254, blank=False)
    description = models.CharField(max_length=254, blank=True)
    body = models.TextField(blank=True)
    create_date = models.DateTimeField(
        "Date created", auto_now_add=True
    )
    pub_date = models.DateTimeField("Date published", blank=True, null=True)
    is_published = models.BooleanField("Published?", default=False)
    author = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.SET_NULL,
    )
    image = models.ImageField(upload_to='img/%Y/%m/', blank=True, null=True)
    tags = TaggableManager(blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Entries"
        ordering = ['-pub_date']

    def get_absolute_url(self):
        return reverse('entry_details', kwargs={
            'year': self.pub_date.year,
            'month': self.pub_date.strftime('%m'),
            'day': self.pub_date.strftime('%d'),
            'slug': self.slug})

    def save(self, *args, **kwargs):
        if not self.pk:
            if self.is_published or self.pub_date:
                self.pub_date = self.pub_date or timezone.now()
                self.is_published = True
            return super(Entry, self).save(*args, **kwargs)
        if not self.is_published:
            self.pub_date = None
        if self.is_published and not self.pub_date:
            self.pub_date = timezone.now()
        super(Entry, self).save(*args, **kwargs)