from django.conf import settings

if 'taggit' not in settings.INSTALLED_APPS:
    raise Exception("This module requires taggit app."
                    "Please install it with pip install django-taggit")