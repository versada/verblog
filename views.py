from django.views.generic.detail import DetailView
from models import Entry
from taggit.models import Tag
from django.http import Http404
from django.utils.translation import ugettext as _
from django.db.models.query import Q

# enable django-endless-pagination features
try:
    from endless_pagination.views import AjaxListView as ListView
except ImportError:
    from django.views.generic.list import ListView
from django.core.urlresolvers import resolve, reverse


class CommonContextMixin(object):
    def get_context_data(self, **kwargs):
        context = super(CommonContextMixin, self).get_context_data(**kwargs)
        context['tag_list'] = Tag.objects.all()
        context['archive_dates_list'] = Entry.objects.filter(
            is_published=True).datetimes('pub_date', 'month', order='DESC')[:10]
        return context


class ListviewMixin(object):
    def get_context_data(self, **kwargs):
        # put variable "go_back_url" into session
        context = super(ListviewMixin, self).get_context_data(**kwargs)
        url = self.request.path
        if self.request.GET.get('q'):
            url += '?q=%s' % self.request.GET['q']
        self.request.session['go_back_url'] = url
        return context


class DetailViewMixin(object):
    def get_context_data(self, **kwargs):
        context = super(DetailViewMixin, self).get_context_data(**kwargs)
        back_url = self.request.session.get('go_back_url', None)
        if not back_url:
            back_url = reverse('blog_index')
        context['go_back_url'] = back_url
        return context


class EntryListView(ListviewMixin, CommonContextMixin, ListView):
    model = Entry

    def get_queryset(self):
        entry_filter = Q(is_published=True)
        kwargs = self.kwargs
        if 'tag' in kwargs:
            entry_filter = entry_filter & Q(tags__name=kwargs['tag'])

        if 'year' in kwargs and 'month' in kwargs:
            entry_filter = (entry_filter &
                            Q(pub_date__year=kwargs['year'],
                              pub_date__month=kwargs['month']))
        if self.request.GET.get('q', False):
            q = self.request.GET['q']
            entry_filter = (entry_filter & (
                Q(title__icontains=q) |
                Q(description__icontains=q) |
                Q(body__icontains=q)))
        return Entry.objects.filter(entry_filter)


class EntryDetailView(CommonContextMixin, DetailViewMixin, DetailView):
    model = Entry

    def get_object(self):
        obj = super(EntryDetailView, self).get_object()
        if not obj.is_published and not self.request.user.is_authenticated():
            raise Http404(_("No %(verbose_name)s found matching the query") %
                          {'verbose_name': self.model._meta.verbose_name})
        return obj
