from django.conf.urls import patterns, url, include
from views import EntryListView, EntryDetailView

urlpatterns = patterns('verblog.views',
    url(r'^$',
        EntryListView.as_view(),
        name='blog_index'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/$',
        EntryListView.as_view(),
        name='blog_index_range'),
    url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/'
        r'(?P<slug>[-_\w]+)/$',
        EntryDetailView.as_view(),
        name='entry_details'),
    url(r'^(?P<tag>[-_\w]+)/$',
        EntryListView.as_view(),
        name='blog_index_tags'),
)
urlpatterns += patterns('',
    url(r'^autocomplete/', include('autocomplete_light.urls')),
)