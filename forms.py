from django import forms
from autocomplete_light.contrib import taggit_field


class EntryForm(forms.ModelForm):
    tags = taggit_field.TaggitField(
        widget=taggit_field.TaggitWidget('TagAutocomplete'), required=False)
